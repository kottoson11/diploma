<h1>Платформа</h1>

<p>Описание проекта</p>
<hr/>

## Установка зависимостей (используемые библиотеки)

Установите необходимые зависимости

```bash
npm i --legacy-peer-deps
```

В проекте используется следующие библиотеки:
- [react-bootstrap](https://react-bootstrap.github.io/) - фреймворк для дизайна.

## Запуск проекта

Для запуска проекта

```bash
npm run dev
```

по умолчанию, проект откроется [на локальном хосте (http://localhost:3000/)](http://localhost:3000/)

## Авторы

Проект разработан студентами:
- Студент 1;
- Студент 2.

