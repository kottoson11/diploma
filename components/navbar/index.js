import {useState} from "react"
import {Modal} from "react-bootstrap"
import ReCAPTCHA from "react-google-recaptcha"
import { Nav } from "components";
import { userService } from 'services';

export default function Header() {

    function onChange(value) {
        console.log("Captcha value:", value);
    }

    function MyVerticallyCenteredModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Вход
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-group">
                        <div className="form-floating mb-3">
                            <input type="email" className="form-control" id="floatingInput"
                                   placeholder="name@example.com"/>
                            <label htmlFor="floatingInput">Логин</label>
                        </div>
                        <div className="form-floating">
                            <input type="password" className="form-control" id="floatingPassword"
                                   placeholder="Password"/>
                            <label htmlFor="floatingPassword">Пароль</label>
                        </div>
                    </div>
                    <div className={"pt-4"}>
                        <ReCAPTCHA
                            sitekey="Your client site key"
                            onChange={onChange}
                        />
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <button onClick={props.onHide} className={"btn btn-primary"} style={{width: "100px"}}>Войти</button>
                    <button onClick={props.onHide} className={"btn btn-outline-primary"}
                            style={{width: "100px"}}>Закрыть
                    </button>
                </Modal.Footer>
            </Modal>
        );
    }

    const [modalShow, setModalShow] = useState(false)

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top">

                <div className="container">
                    <a className="navbar-brand" href="/start">Тестовый стенд</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarColor01"
                            aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarColor01">

                        <ul className="navbar-nav me-auto">
                            <li className="nav-item active">
                                <a className="nav-link" href={"/allCard"}>Все организации</a>
                            </li>
                                {/* <a className="nav-link" href={"/allCard"}>Выйти</a> */}
                        </ul>

                        <div className="d-flex">

                            {!userService.userValue?.firstName ? "": <button type="button" className="btn btn-primary"
                                    onClick={() => userService.logout()}>{userService.userValue?.firstName}{"  "}
                                <svg
                                    xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    className="bi bi-box-arrow-right" viewBox="0 0 16 16" style={{marginBottom: "2px"}}>
                                    <path fillRule="evenodd"
                                          d="M10 12.5a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-9a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v2a.5.5 0 0 0 1 0v-2A1.5 1.5 0 0 0 9.5 2h-8A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-2a.5.5 0 0 0-1 0v2z"/>
                                    <path fillRule="evenodd"
                                          d="M15.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 0 0-.708.708L14.293 7.5H5.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                                    `
                                </svg>
                            </button>}
                        </div>

                    </div>

                </div>

            </nav>

            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
            />
        </>
    )
}
