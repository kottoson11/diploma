import Nav from "../navbar"
import BasicChart from "../chart/chart"
import BasicBar from "../chart/bar"

export default function Card(data) {
    return (
        <>
            <Nav/>

            <div className={"container"}>

                <div className={"row g-0"}>
                    <div className={"col-sm-9 pe-5"}>
                        <div className={"card pt-4 p-5"} style={{borderRadius: "0", borderTop: "none"}}>
                            <h1>{data.company.name_company}</h1>

                            <p className={"mt-4 pb-3"} style={{maxWidth: "50rem"}}>
                                {data.company.description_company}
                            </p>
                            <hr/>
                            <div className={"row pt-3 pb-3"}>
                                <div className={"col-sm"}>
                                    <img src={data.company.avatar_company} alt={data.company.name_avatar} width={250}
                                         height={250} style={{borderRadius: "20px", border: "1px solid #00000033"}}/>
                                </div>
                                <div className={"col-sm-8"}>
                                    <div className={"pb-2"}><b>Руководитель</b></div>
                                    <p>{data.leader.first_name_leader}{" "}{data.leader.last_name_leader}</p>
                                    <div><b>О себе</b></div>
                                    <p>{data.leader.about_leader}</p>
                                </div>
                            </div>

                            <hr/>
                            <BasicBar/>
                            <hr/>
                            <p className={"pt-3 pb-3"} style={{maxWidth: "50rem"}}>
                                {data.other_text.first_diagram}
                            </p>
                            <hr/>
                            <BasicChart/>
                            <hr/>
                            <p className={"pt-3 pb-3"} style={{maxWidth: "50rem"}}>
                                {data.other_text.second_diagram}
                            </p>

                        </div>
                    </div>

                    <div className={"col mt-3"}>
                        <div className={"sticky-top"} style={{top: "80px"}}>
                            <div className="card mb-5" style={{maxWidth: "20rem"}}>
                                <h5 className="card-header">Контакты{"  "}
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                         height="16" fill="currentColor"
                                         className="bi bi-person-lines-fill"
                                         viewBox="0 0 16 16">
                                        <path
                                            d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
                                    </svg>
                                </h5>
                                <div className="card-body">
                                    <div className="card-text">Номер телефона: <a
                                        href={`mailto:${"+7" + data.contact_us.phone}`}>+7{data.contact_us.phone}</a>
                                    </div>
                                    <div className="card-text">E-mail: <a
                                        href={""}>{data.contact_us.email}</a></div>
                                </div>
                            </div>
                            <div className="card" style={{maxWidth: "20rem"}}>
                                <h5 className="card-header">Месторасположение{"  "}
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor"
                                         className="bi bi-geo-alt-fill" viewBox="0 0 16 16">
                                        <path
                                            d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
                                    </svg>
                                </h5>

                                <img src={"/map.svg"} alt={"map"}/>
                                <div className="card-body">
                                    <div className="card-text">Страна: {data.contact_us.country}</div>
                                    <div className="card-text">Населеный пункт: {data.contact_us.locality}</div>
                                    <div className="card-text">Улица: {data.contact_us.street} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}