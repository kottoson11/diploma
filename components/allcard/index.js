import Link from "next/link"


export default function AllCard(result) {
    return (
        <>
        <div className={"container mt-4"}>

            <h1>Список коммерческих организаций</h1>

            <div className={'container row no-gutters row-cols-lg-3 g-0 mt-4'}>

                {result.data.map((data, index) =>

                    <div className={"col p-2"} key={index}>
                        <div className="card prev">
                            <div className="card-body">
                                <h4 className="card-title mb-3"><Link href={`/card/${index + 1}`}>{data.company}</Link>
                                </h4>
                                {/*<h6 className="card-subtitle mb-2 text-muted">Род деятельности</h6>*/}
                                <p className="card-text">{data.about}</p>
                                <div className={"bottomWrapper"}/>
                                <div className={"bottom"}><a href={`/card/${index + 1}`} className="card-link">Подробнее
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         className="bi bi-arrow-right-short" viewBox="0 0 16 16">
                                        <path fillRule="evenodd"
                                              d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                    </svg>
                                </a>
                                </div>
                            </div>
                        </div>
                    </div>
                )}

            </div>

            <div className={"d-flex justify-content-center mt-4"}>
                <ul className="pagination">
                    <li className="page-item disabled">
                        <a className="page-link" href="#">&laquo;</a>
                    </li>
                    <li className="page-item active">
                        <a className="page-link" href="#">1</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">2</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">3</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">&raquo;</a>
                    </li>
                </ul>
            </div>

        </div>
        </>
    )
}