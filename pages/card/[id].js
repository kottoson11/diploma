import faker from "@faker-js/faker/locale/ru"
import Card from "../../components/card"


export default function Index({ data }) {
    return <Card {...data} />;
}


export async function getServerSideProps() {
    // Fetch data from external API
    const data = {
        company: {
            name_company: faker.company.companyName(),
            description_company: faker.lorem.paragraph(),
            avatar_company: faker.image.avatar(),
            avatar_name: faker.name.firstName(),
        },
        leader: {
            first_name_leader: faker.name.firstName(),
            last_name_leader: faker.name.lastName(),
            about_leader: faker.lorem.paragraph(), 
        },
        other_text: {
            first_diagram: faker.lorem.paragraph() + faker.lorem.paragraph(),
            second_diagram: faker.lorem.paragraph() + faker.lorem.paragraph()
        },
        contact_us: {
            email: faker.internet.email(),
            country: faker.address.city(),
            locality: faker.address.city(),
            street: faker.address.city(),
            phone: faker.phone.phoneNumber()
        }
    }

    // Pass data to the page via props
    return { props: { data } }
  }