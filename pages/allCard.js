import Head from "next/head"
import AllCard from "../components/allcard"
import React from 'react'
import Nav from "../components/navbar"
import faker from "@faker-js/faker/locale/ru"

export default function index({ result }) {
    return (
        <>
            <Head>
                <title>Все организации</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Nav/>

            <AllCard {...result}/>
        </>
    )
}

export async function getServerSideProps() {
    // Fetch data from external API
    const data = [
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()},
        {company: faker.company.companyName(), about: faker.lorem.paragraph()}
    ]
  
    // Pass data to the page via props
    return { props: { 
        result: {
            data
        }
     } }
  }