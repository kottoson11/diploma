// import { userService } from 'services';

import Head from 'next/head'
import Link from "next/link"
import Header from "../../components/navbar"


export default function Index() {

    function checkBrowser(browser) {
        if (browser) {
            if (localStorage.user) {
                return true
            }
        } else {
            return false
        }
    }

    return (
        <>
            <Head>
                <title>Дипломный проект - тестовый стенд</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Header />

            <div
                className={"container d-flex justify-content-center align-content-center align-items-center justify-items-center"}
                style={{ minHeight: "70vh" }}>
                <div className={"card border-primary  p-5"} style={{ maxWidth: "750px", width: "100%" }}>
                    <h1 className={"mb-5 mt-5"}>Выберите профиль</h1>
                    <div className={"container mb-5"}>
                        <div className={"row gap-5"}>
                            <Link href={"/allCard"}>
                                <button className="btn btn-lg btn-primary col" type="button"
                                    style={{ maxWidth: "300px" }}>
                                    Без личного кабинета
                                </button>
                            </Link>
                            {checkBrowser(process.browser) == true 
                                ?
                                <Link href={"/allCard"}>
                                    <button className="btn btn-lg btn-primary col" type="button"
                                        style={{ maxWidth: "300px" }}>С личным
                                        кабинетом
                                    </button>
                                </Link>
                                :
                                <Link href={"/authorization"}>
                                    <button className="btn btn-lg btn-primary col" type="button"
                                        style={{ maxWidth: "300px" }}>С личным
                                        кабинетом
                                    </button>
                                </Link>}
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}